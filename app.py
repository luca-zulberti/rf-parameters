#!/usr/bin/env python3
import sys
from cmath import rect, pi

from PySide2.QtWidgets import QApplication

from lib.qt.QRfApp import QRfApp
from lib.rf.devices import YDev, SDev, YTest, STest

#
# Constants
#

version = '1.3.2'


#
# Local variables
#

devices = []  # Initial devices

yi = 0.0045 + 0.0095j
yr = -0.0007j
yf = 0.045 - 0.032j
yo = 0.0002 + 0.0023j
devices.append(YDev(yi, yr, yf, yo, '2N4957_CE@300MHz'))

yi = 0.054 - 0.021j
yr = 0
yf = -0.048 + 0.025j
yo = 0.00008 + 0.0022j
devices.append(YDev(yi, yr, yf, yo, '2N4957_CB@300MHz'))

s11 = rect(0.62, -143 * pi / 180)
s21 = rect(5.5, 97 * pi / 180)
s12 = rect(0.08, 33 * pi / 180)
s22 = rect(0.41, -59 * pi / 180)
NFmin = 10**(0.9 / 10)
Rn = 9.3 / 50
gammaSoptN = rect(0.49, 74 * pi / 180)
devices.append(SDev(s11, s21, s12, s22, NFmin,
                    Rn, gammaSoptN, 'MRF571@500MHz'))

s11 = rect(0.61, 178 * pi / 180)
s21 = rect(3.0, 78 * pi / 180)
s12 = rect(0.09, 37 * pi / 180)
s22 = rect(0.28, -69 * pi / 180)
NFmin = 10**(1.5 / 10)
Rn = 7.5 / 50
gammaSoptN = rect(0.48, 134 * pi / 180)
devices.append(SDev(s11, s21, s12, s22, NFmin, Rn, gammaSoptN, 'MRF571@1GHz'))

s11 = rect(0.65, 158 * pi / 180)
s21 = rect(2.0, 62 * pi / 180)
s12 = rect(0.11, 44 * pi / 180)
s22 = rect(0.26, -88 * pi / 180)
NFmin = 10**(1.5 / 10)
Rn = 7.5 / 50
gammaSoptN = rect(0.48, 134 * pi / 180)
devices.append(SDev(s11, s21, s12, s22, NFmin,
                    Rn, gammaSoptN, 'MRF571@1.5GHz'))

tests = []  # Initial tests

ys = 0.02
yl = 0.02
tests.append(YTest(ys, yl, 'test1'))

gammaS = rect(0, 0)
gammaL = rect(0, 0)
Ga = 1.0
Gp = 1.0
Gt = 1.0
NF = 1.0
tests.append(STest(gammaS, gammaL, Ga, Gp, Gt, NF, name='test1'))

#
# Application
#

app = QApplication(sys.argv)
rfApp = QRfApp(version)

for dev in devices:
    rfApp.addDev(dev)

for test in tests:
    rfApp.addTest(test)

sys.exit(app.exec_())  # Start the application
