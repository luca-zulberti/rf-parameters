Y e S parameters Spreadsheet
============================

Dependency
----------

Python3 and pip3 need to be installed.

Use with Linux
--------------

Install requirements:

```$ pip3 install -r requirements.txt```

Start application

```$ ./app.py```


Use with Windows
----------------

Install requirements:

```> \.install_requirements.bat```

Start application

```> \.launch.bat```
