#!/usr/bin/env python3
from lib.rf.yparam import YDev

yi = 0.02
yr = 0.03 + 0.01j
yf = 0.01 - 0.005j
yo = 0.02 + 0.001j

ys = 0.02
yl = 0.02

dev = YDev(yi, yr, yf, yo)

print(f'Yi: {yi * 1000:.2f}')
print(f'Yo: {yo * 1000:.2f}')
print(f'Yr: {yr * 1000:.2f}')
print(f'Yf: {yf * 1000:.2f}')

print('')

print(f'Ys: {ys * 1000:.2f}')
print(f'Yl: {yl * 1000:.2f}')

print('')

print(f'Ysopt: {dev.ysopt * 1000:.2f}')
print(f'Ylopt: {dev.ylopt * 1000:.2f}')

print('')

print(f'C: {dev.c:.3f}')
print(f'Yin: {dev.yin(yl) * 1000:.3f}')
print(f'Yout: {dev.yout(ys) * 1000:.3f}')
print(f'Ga: {dev.ga(ys):.3f}')
print(f'Gp: {dev.gp(yl):.3f}')
print(f'Gt: {dev.gt(ys, yl):.3f}')
print(f'K: {dev.k(ys, yl):.3f}')
