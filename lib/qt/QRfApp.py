from PySide2.QtWidgets import QMainWindow, QDockWidget, QTabWidget, QWidget, \
    QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QFrame, QSizePolicy
from PySide2.QtCore import Qt, QCoreApplication, QObject, Slot, SIGNAL

from lib.rf.devices import Dev, YDev, SDev, Test, YTest, STest
from lib.qt.QRfLib import QRfSDevForm, QRfYDevForm, QRfYTestForm, \
    QRfSTestForm, QRfYResultForm, QRfSResultForm, QRfList, QRfItem


class QRfApp(QMainWindow):

    def __init__(self, version='0.0.1'):
        super().__init__()

        self.initUi(version)

    def initUi(self, version):
        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)

        # Create application widgets
        self.contentY = QRfYContent()
        self.contentS = QRfSContent()

        self.contentTab = QTabWidget()
        self.contentTab.addTab(self.contentY, self.tr('Y Parameters'))
        self.contentTab.addTab(self.contentS, self.tr('S Parameters'))
        self.contentTab.currentChanged.connect(self.curTabChange)
        self.curTabChange(0)

        # Create Menu widget
        self.menu = self.menuBar()

        fileMenu = self.menu.addMenu('&File')
        quitAct = fileMenu.addAction('&Quit')
        QObject.connect(quitAct, SIGNAL("triggered()"), self.quit)

        # Add to MainWindow
        self.setCentralWidget(self.contentTab)

        self.setWindowTitle(
            self.tr(f'Y / S parameters Calculator - v{version}'))

        self.show()

    def addDev(self, dev):
        if type(dev) is YDev:
            self.contentY.addDev(dev)
        elif type(dev) is SDev:
            self.contentS.addDev(dev)

    def addTest(self, test):
        if type(test) is YTest:
            self.contentY.addTest(test)
        elif type(test) is STest:
            self.contentS.addTest(test)

    @Slot()
    def quit(self):
        QCoreApplication.quit()

    @Slot(int)
    def curTabChange(self, index):
        for i in range(self.contentTab.count()):
            if i == index:
                self.contentTab.widget(i).setSizePolicy(
                    QSizePolicy.Preferred, QSizePolicy.Preferred)
            else:
                self.contentTab.widget(i).setSizePolicy(
                    QSizePolicy.Ignored, QSizePolicy.Ignored)

        self.resize(self.minimumSizeHint())


class QRfContent(QWidget):

    def __init__(self):
        super().__init__()

        self.initUi()

    def initUi(self, devForm, testForm, resultForm):
        self.devForm = devForm
        self.devForm.setSizePolicy(QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        self.devsList = QRfList()
        self.devsList.itemSelected.connect(self.changeDev)
        self.addDevBtn = QPushButton(self.tr('New device'))
        self.addDevBtn.clicked.connect(self.newDev)

        self.testForm = testForm
        self.testForm.setSizePolicy(QSizePolicy.Expanding,
                                    QSizePolicy.Expanding)
        self.testsList = QRfList()
        self.testsList.itemSelected.connect(self.changeTest)
        self.addTestBtn = QPushButton(self.tr('New test'))
        self.addTestBtn.clicked.connect(self.newTest)

        self.resultForm = resultForm

        self.devsLayout = QHBoxLayout()
        self.devsListLayout = QVBoxLayout()
        self.devsListLayout.addWidget(self.addDevBtn)
        self.devsListLayout.addWidget(self.devsList)
        self.devsLayout.addLayout(self.devsListLayout)
        self.devsLayout.addWidget(self.devForm)

        self.testsLayout = QHBoxLayout()
        self.testsListLayout = QVBoxLayout()
        self.testsListLayout.addWidget(self.addTestBtn)
        self.testsListLayout.addWidget(self.testsList)
        self.testsLayout.addLayout(self.testsListLayout)
        self.testsLayout.addWidget(self.testForm)

        lineData = QFrame()
        lineData.setFrameShape(QFrame.VLine)
        lineData.setFrameShadow(QFrame.Sunken)

        self.dataLayout = QHBoxLayout()
        self.dataLayout.addLayout(self.devsLayout)
        self.dataLayout.addWidget(lineData)
        self.dataLayout.addLayout(self.testsLayout)

        self.calcBtn = QPushButton('Calculate')
        self.calcBtn.clicked.connect(self.displayResults)

        self.resultHeaderLayout = QHBoxLayout()
        self.resultHeaderLayout.addWidget(QLabel('Results'))
        self.resultHeaderLayout.addStretch(1)
        self.resultHeaderLayout.addWidget(self.calcBtn)

        self.resultLayout = QVBoxLayout()
        self.resultLayout.addLayout(self.resultHeaderLayout)
        self.resultLayout.addWidget(self.resultForm)

        lineRes = QFrame()
        lineRes.setFrameShape(QFrame.HLine)
        lineRes.setFrameShadow(QFrame.Sunken)

        self.layout = QVBoxLayout(self)
        self.layout.addLayout(self.dataLayout)
        self.layout.addWidget(lineRes)
        self.layout.addLayout(self.resultLayout)

    def replaceDevForm(self, w):
        item = self.devsLayout.replaceWidget(self.devForm, w)
        item.widget().deleteLater()

        self.devForm = w
        w.devSaved.connect(self.saveDev)

    def replaceTestForm(self, w):
        item = self.testsLayout.replaceWidget(self.testForm, w)
        item.widget().deleteLater()

        self.testForm = w
        w.testSaved.connect(self.saveTest)

    def replaceResultForm(self, w):
        item = self.resultLayout.replaceWidget(self.resultForm, w)
        item.widget().deleteLater()

        self.resultForm = w

    def addDev(self, dev):
        self.devsList.addItem(dev)

    def addTest(self, test):
        self.testsList.addItem(test)

    @Slot(Dev)
    def changeDev(self, dev):
        self.selectedDev = dev

    @Slot(Dev)
    def saveDev(self, dev):
        if self.selectedDev:
            self.devsList.updateItem(dev)
        else:
            self.devsList.addItem(dev)

    @Slot()
    def newDev(self):
        self.selectedDev = None
        self.devsList.clearSelection()

    @Slot(Test)
    def changeTest(self, test):
        self.selectedTest = test

    @Slot(Test)
    def saveTest(self, test):
        if self.selectedTest:
            self.testsList.updateItem(test)
        else:
            self.testsList.addItem(test)

    @Slot()
    def newTest(self):
        self.selectedTest = None
        self.testsList.clearSelection()

    @Slot()
    def displayResults(self):
        pass


class QRfYContent(QRfContent):

    def __init__(self):
        super().__init__()

    def initUi(self):
        devForm = QRfYDevForm()
        testForm = QRfYTestForm()
        resultForm = QRfYResultForm()
        super().initUi(devForm, testForm, resultForm)

    @Slot(QRfItem)
    def changeDev(self, item):
        dev = item.item
        super().changeDev(dev)
        self.replaceDevForm(QRfYDevForm(dev))

    @Slot()
    def newDev(self):
        super().newDev()
        self.replaceDevForm(QRfYDevForm())

    @Slot(QRfItem)
    def changeTest(self, item):
        test = item.item
        super().changeTest(test)
        self.replaceTestForm(QRfYTestForm(test))

    @Slot()
    def newTest(self):
        super().newTest()
        self.replaceTestForm(QRfYTestForm())

    @Slot()
    def displayResults(self):
        self.replaceResultForm(QRfYResultForm(
            self.devForm.dev,
            self.testForm.test
        ))


class QRfSContent(QRfContent):

    def __init__(self):
        super().__init__()

    def initUi(self):
        devForm = QRfSDevForm()
        testForm = QRfSTestForm()
        resultForm = QRfSResultForm()
        super().initUi(devForm, testForm, resultForm)

    @Slot(QRfItem)
    def changeDev(self, item):
        dev = item.item
        super().changeDev(dev)
        self.replaceDevForm(QRfSDevForm(dev))

    @Slot()
    def newDev(self):
        super().newDev()
        self.replaceDevForm(QRfSDevForm())

    @Slot(QRfItem)
    def changeTest(self, item):
        test = item.item
        super().changeTest(test)
        self.replaceTestForm(QRfSTestForm(test))

    @Slot()
    def newTest(self):
        super().newTest()
        self.replaceTestForm(QRfSTestForm())

    @Slot()
    def displayResults(self):
        self.replaceResultForm(QRfSResultForm(
            self.devForm.dev,
            self.testForm.test
        ))
