from math import log10
from cmath import rect, pi, phase
from re import split
from enum import Enum

from PySide2.QtCore import Qt, Signal, Slot
from PySide2.QtWidgets import QWidget, QFrame, \
    QLabel, QLineEdit, QListWidget, QListWidgetItem, QPushButton, \
    QMessageBox, QHBoxLayout, QVBoxLayout, QGridLayout, QSizePolicy

from lib.rf.devices import Circle, Dev, YDev, SDev, Test, YTest, STest


def printPolar(c):
    return f'{abs(c):.3f} / {phase(c) * 180 / pi:.1f}\u00b0'


class ValueType(Enum):
    STRING = 1
    NUMBER = 2
    COMPLEX_RECT = 3
    COMPLEX_POLAR = 4
    CIRCLE = 5


class QRfDevForm(QWidget):
    devSaved = Signal(Dev)

    def __init__(self, dev=None):
        if dev is None:
            dev = Dev()
        super().__init__()

        self.dev = dev

        self.initUi()

    def initUi(self):
        self.name = QRfParam(self.tr('Name'), self.dev.name)

        if self.dev.name == '':
            self.label = QLabel(self.tr('New Y device'))
            self.saveBtn = QPushButton(self.tr('Add'))
        else:
            self.label = QLabel(self.tr('Modify Y device'))
            self.saveBtn = QPushButton(self.tr('Save'))

        self.saveBtn.clicked.connect(self.saveDev)

        self.rootLayout = QVBoxLayout(self)
        self.rootLayout.addWidget(self.label)
        self.rootLayout.addWidget(self.name)

        self.footerLayout = QHBoxLayout()
        self.footerLayout.addWidget(self.saveBtn)
        self.footerLayout.setAlignment(Qt.AlignRight)

    def updateUi(self):
        self.name.updateVal(self.dev.name)

    @Slot()
    def saveDev(self):
        name = self.name.getValue()
        if name == '':
            QMessageBox.warning(self, self.tr('Warning'),
                                self.tr('Name cannot be null (\'\')'),
                                QMessageBox.Ok)
            return
        self.dev.name = name

        self.updateUi()
        self.devSaved.emit(self.dev)


class QRfYDevForm(QRfDevForm):

    def __init__(self, dev=None):
        if dev is None:
            dev = YDev()
        super().__init__(dev)

    def initUi(self):
        super().initUi()

        self.qYi = QRfParam('Yi', self.dev.yi, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)
        self.qYr = QRfParam('Yr', self.dev.yr, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)
        self.qYf = QRfParam('Yf', self.dev.yf, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)
        self.qYo = QRfParam('Yo', self.dev.yo, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)

        self.paramsLayout = QGridLayout()
        self.paramsLayout.addWidget(self.qYi, 0, 0)
        self.paramsLayout.addWidget(self.qYr, 0, 1)
        self.paramsLayout.addWidget(self.qYf, 1, 0)
        self.paramsLayout.addWidget(self.qYo, 1, 1)

        self.rootLayout.addLayout(self.paramsLayout)
        self.rootLayout.addStretch(1)
        self.rootLayout.addLayout(self.footerLayout)

    def updateUi(self):
        super().updateUi()
        self.qYi.updateVal(self.dev.yi)
        self.qYr.updateVal(self.dev.yr)
        self.qYf.updateVal(self.dev.yf)
        self.qYo.updateVal(self.dev.yo)

    @Slot()
    def saveDev(self):
        self.dev.yi = self.qYi.getValue()
        self.dev.yr = self.qYr.getValue()
        self.dev.yf = self.qYf.getValue()
        self.dev.yo = self.qYo.getValue()
        super().saveDev()


class QRfSDevForm(QRfDevForm):

    def __init__(self, dev=None):
        if dev is None:
            dev = SDev()
        super().__init__(dev)

    def initUi(self):
        super().initUi()

        self.qS11 = QRfParam('S11', self.dev.s11,
                             valType=ValueType.COMPLEX_POLAR)
        self.qS12 = QRfParam('S12', self.dev.s12,
                             valType=ValueType.COMPLEX_POLAR)
        self.qS21 = QRfParam('S21', self.dev.s21,
                             valType=ValueType.COMPLEX_POLAR)
        self.qS22 = QRfParam('S22', self.dev.s22,
                             valType=ValueType.COMPLEX_POLAR)
        self.qNFmin = QRfParam('NFmin', self.dev.NFmin,
                               valType=ValueType.NUMBER, unit='dB', scale='dB')
        self.qRn = QRfParam('Rn (normalized)', self.dev.Rn,
                            valType=ValueType.NUMBER)
        self.qGamSon = QRfParam('\u0393s Opt Noise', self.dev.gammaSoptN,
                                valType=ValueType.COMPLEX_POLAR)

        self.paramsLayout = QGridLayout()
        self.paramsLayout.addWidget(self.qS11, 0, 0)
        self.paramsLayout.addWidget(self.qS12, 0, 1)
        self.paramsLayout.addWidget(self.qS21, 1, 0)
        self.paramsLayout.addWidget(self.qS22, 1, 1)

        self.rootLayout.addLayout(self.paramsLayout)
        self.rootLayout.addWidget(self.qNFmin)
        self.rootLayout.addWidget(self.qRn)
        self.rootLayout.addWidget(self.qGamSon)
        self.rootLayout.addStretch(1)
        self.rootLayout.addLayout(self.footerLayout)

    def updateUi(self):
        super().updateUi()
        self.qS11.updateVal(self.dev.s11)
        self.qS12.updateVal(self.dev.s12)
        self.qS21.updateVal(self.dev.s21)
        self.qS22.updateVal(self.dev.s22)
        self.qNFmin.updateVal(self.dev.NFmin)
        self.qRn.updateVal(self.dev.Rn)
        self.qGamSon.updateVal(self.dev.gammaSoptN)

    @Slot()
    def saveDev(self):
        self.dev.s11 = self.qS11.getValue()
        self.dev.s12 = self.qS12.getValue()
        self.dev.s21 = self.qS21.getValue()
        self.dev.s22 = self.qS22.getValue()
        self.dev.NFmin = self.qNFmin.getValue()
        self.dev.Rn = self.qRn.getValue()
        self.dev.gammaSoptN = self.qGamSon.getValue()
        super().saveDev()


class QRfTestForm(QWidget):
    testSaved = Signal(Test)

    def __init__(self, test=None):
        if test is None:
            test = Test()
        super().__init__()

        self.test = test

        self.initUi()

    def initUi(self):
        self.name = QRfParam('Name', self.test.name)

        if self.test.name == '':
            self.label = QLabel(self.tr('New Y device'))
            self.saveBtn = QPushButton(self.tr('Add'))
        else:
            self.label = QLabel(self.tr('Modify Y device'))
            self.saveBtn = QPushButton(self.tr('Save'))

        self.saveBtn.clicked.connect(self.saveTest)

        self.rootLayout = QVBoxLayout(self)
        self.rootLayout.addWidget(self.label)
        self.rootLayout.addWidget(self.name)

        self.footerLayout = QHBoxLayout()
        self.footerLayout.addWidget(self.saveBtn)
        self.footerLayout.setAlignment(Qt.AlignRight)

    def updateUi(self):
        self.name.updateVal(self.test.name)

    @Slot()
    def saveTest(self):
        name = self.name.getValue()
        if name == '':
            QMessageBox.warning(self, self.tr('Warning'),
                                self.tr('Name cannot be null (\'\')'),
                                QMessageBox.Ok)
            return
        self.test.name = name

        self.updateUi()
        self.testSaved.emit(self.test)


class QRfYTestForm(QRfTestForm):

    def __init__(self, test=None):
        if test is None:
            test = YTest()
        super().__init__(test)

    def initUi(self):
        super().initUi()

        self.qYs = QRfParam('Ys', self.test.ys, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)
        self.qYl = QRfParam('Yl', self.test.yl, valType=ValueType.COMPLEX_RECT,
                            unit='mS', scale=3)

        self.paramsLayout = QGridLayout()
        self.paramsLayout.addWidget(self.qYs, 0, 0)
        self.paramsLayout.addWidget(self.qYl, 0, 1)

        self.rootLayout.addLayout(self.paramsLayout)
        self.rootLayout.addStretch(1)
        self.rootLayout.addLayout(self.footerLayout)

    def updateUi(self):
        super().updateUi()
        self.qYs.updateVal(self.test.ys)
        self.qYl.updateVal(self.test.yl)

    @Slot()
    def saveTest(self):
        self.test.ys = self.qYs.getValue()
        self.test.yl = self.qYl.getValue()
        super().saveTest()


class QRfSTestForm(QRfTestForm):

    def __init__(self, test=None):
        if test is None:
            test = STest()
        super().__init__(test)

    def initUi(self):
        super().initUi()

        self.qGamS = QRfParam('\u0393s', self.test.gammaS,
                              valType=ValueType.COMPLEX_POLAR)
        self.qGamL = QRfParam('\u0393l', self.test.gammaL,
                              valType=ValueType.COMPLEX_POLAR)
        self.qGa = QRfParam('Ga', self.test.Ga, valType=ValueType.NUMBER,
                            unit='dB', scale='dB')
        self.qGp = QRfParam('Gp', self.test.Gp, valType=ValueType.NUMBER,
                            unit='dB', scale='dB')
        self.qGt = QRfParam('Gt', self.test.Gt, valType=ValueType.NUMBER,
                            unit='dB', scale='dB')
        self.qNF = QRfParam('NF', self.test.NF, valType=ValueType.NUMBER,
                            unit='dB', scale='dB')

        self.paramsLayout = QGridLayout()
        self.paramsLayout.addWidget(self.qGamS, 0, 0)
        self.paramsLayout.addWidget(self.qGamL, 0, 1)

        self.rootLayout.addLayout(self.paramsLayout)
        self.rootLayout.addWidget(self.qGa)
        self.rootLayout.addWidget(self.qGp)
        self.rootLayout.addWidget(self.qGt)
        self.rootLayout.addWidget(self.qNF)
        self.rootLayout.addStretch(1)
        self.rootLayout.addLayout(self.footerLayout)

    def updateUi(self):
        super().updateUi()
        self.qGamS.updateVal(self.test.gammaS)
        self.qGamL.updateVal(self.test.gammaL)
        self.qGa.updateVal(self.test.Ga)
        self.qGp.updateVal(self.test.Gp)
        self.qGt.updateVal(self.test.Gt)
        self.qNF.updateVal(self.test.NF)

    @Slot()
    def saveTest(self):
        self.test.gammaS = self.qGamS.getValue()
        self.test.gammaL = self.qGamL.getValue()
        self.test.Ga = self.qGa.getValue()
        self.test.Gp = self.qGp.getValue()
        self.test.Gt = self.qGt.getValue()
        self.test.NF = self.qNF.getValue()
        super().saveTest()


class QRfResultForm(QWidget):

    def __init__(self):
        super().__init__()

        self.initUi()

    def initUi(self):
        self.rootLayout = QVBoxLayout(self)


class QRfYResultForm(QRfResultForm):

    def __init__(self, dev=None, test=None):
        if dev is None:
            dev = YDev()
        if test is None:
            test = YTest()
        self.dev = dev
        self.test = test

        super().__init__()

    def initUi(self):
        super().initUi()

        yin = self.dev.yin(self.test.yl)
        self.qYin = QRfParam(self.tr('Yin'), yin, ValueType.COMPLEX_RECT,
                             unit='mS', scale=3, readonly=True)

        yout = self.dev.yout(self.test.ys)
        self.qYout = QRfParam(self.tr('Yout'), yout, ValueType.COMPLEX_RECT,
                              unit='mS', scale=3, readonly=True)

        self.qYsopt = QRfParam(self.tr('Ys Opt'), self.dev.ysopt,
                               ValueType.COMPLEX_RECT, unit='mS', scale=3,
                               readonly=True)
        self.qYlopt = QRfParam(self.tr('Yl Opt'), self.dev.ylopt,
                               ValueType.COMPLEX_RECT, unit='mS', scale=3,
                               readonly=True)

        self.qC = QRfParam(self.tr('C'), self.dev.c,
                           ValueType.NUMBER, readonly=True)

        k = self.dev.k(self.test.ys, self.test.yl)
        self.qK = QRfParam(self.tr('K'), k, ValueType.NUMBER, readonly=True)

        betaA = self.dev.betaA(self.test.ys, self.test.yl)
        self.qBetaA = QRfParam(self.tr('\u03b2A'), betaA,
                               ValueType.COMPLEX_POLAR, readonly=True)

        ga = self.dev.ga(self.test.ys)
        self.qGa = QRfParam(self.tr('Ga'), ga, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        gp = self.dev.gp(self.test.yl)
        self.qGp = QRfParam(self.tr('Gp'), gp, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        gt = self.dev.gt(self.test.ys, self.test.yl)
        self.qGt = QRfParam(self.tr('Gt'), gt, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        aV = self.dev.aV(self.test.yl)
        self.qAv = QRfParam(self.tr('Av'), aV,
                            ValueType.COMPLEX_POLAR, readonly=True)

        aI = self.dev.aI(self.test.yl)
        self.qAi = QRfParam(self.tr('Ai'), aI,
                            ValueType.COMPLEX_POLAR, readonly=True)

        self.paramsLayout = QGridLayout()
        self.paramsLayout.setSpacing(0)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qYin, 0, 0)
        self.paramsLayout.addWidget(self.qYout, 1, 0)
        self.paramsLayout.addWidget(line, 0, 1, 2, 1)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qYsopt, 0, 2)
        self.paramsLayout.addWidget(self.qYlopt, 1, 2)
        self.paramsLayout.addWidget(line, 0, 3, 3, 1)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qK, 0, 4)
        self.paramsLayout.addWidget(self.qC, 1, 4)
        self.paramsLayout.addWidget(self.qBetaA, 2, 4)
        self.paramsLayout.addWidget(line, 0, 5, 3, 1)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qGa, 0, 6)
        self.paramsLayout.addWidget(self.qGp, 1, 6)
        self.paramsLayout.addWidget(self.qGt, 2, 6)
        self.paramsLayout.addWidget(line, 0, 7, 3, 1)

        self.paramsLayout.addWidget(self.qAv, 0, 8)
        self.paramsLayout.addWidget(self.qAi, 1, 8)

        self.rootLayout.addLayout(self.paramsLayout)


class QRfSResultForm(QRfResultForm):

    def __init__(self, dev=None, test=None):
        if dev is None:
            dev = SDev()
        if test is None:
            test = STest()
        self.dev = dev
        self.test = test

        super().__init__()

    def initUi(self):
        super().initUi()

        gSopt = self.dev.gammaSopt
        self.qGso = QRfParam(self.tr('\u0393s Opt'), gSopt,
                             ValueType.COMPLEX_POLAR, readonly=True)

        gLopt = self.dev.gammaLopt
        self.qGlo = QRfParam(self.tr('\u0393l Opt'), gLopt,
                             ValueType.COMPLEX_POLAR, readonly=True)

        gIn = self.dev.gammaIn(self.test.gammaL)
        self.qGin = QRfParam(self.tr('\u0393in'), gIn,
                             ValueType.COMPLEX_POLAR, readonly=True)

        gOut = self.dev.gammaOut(self.test.gammaS)
        self.qGout = QRfParam(self.tr('\u0393out'), gOut,
                              ValueType.COMPLEX_POLAR, readonly=True)

        d = self.dev.D
        self.qD = QRfParam(self.tr('D'), d,
                           ValueType.COMPLEX_POLAR, readonly=True)

        k = self.dev.K
        self.qK = QRfParam(self.tr('K'), k,
                           ValueType.NUMBER, readonly=True)

        ga = self.dev.ga(self.test.gammaS)
        self.qGa = QRfParam(self.tr('Ga'), ga, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        gp = self.dev.gp(self.test.gammaL)
        self.qGp = QRfParam(self.tr('Gp'), gp, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        gt = self.dev.gt(self.test.gammaS, self.test.gammaL)
        self.qGt = QRfParam(self.tr('Gt'), gt, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        maxg = self.dev.maxG
        self.qMaxG = QRfParam(self.tr('Max G'), maxg, ValueType.NUMBER,
                              unit='dB', scale='dB', readonly=True)

        nf = self.dev.nf(self.test.gammaS)
        self.qNf = QRfParam(self.tr('NF'), nf, ValueType.NUMBER,
                            unit='dB', scale='dB', readonly=True)

        self.qStCircIn = QRfParam(self.tr('Input Stability Circle'),
                                  self.dev.inputSC, ValueType.CIRCLE,
                                  readonly=True)
        QLabel(self.tr(
            f'In StabCirc:\tC: {printPolar(self.dev.inputSC.center)}'
            f'\tR: {self.dev.inputSC.radius:.3f}'
        ))

        self.qStCircOut = QRfParam(self.tr('Output Stability Circle'),
                                   self.dev.outputSC, ValueType.CIRCLE,
                                   readonly=True)

        self.qEquiGa = QRfParam(self.tr('Equi Ga'), self.dev.equiGa(ga),
                                ValueType.CIRCLE, readonly=True)

        self.qEquiGp = QRfParam(self.tr('Equi Gp'), self.dev.equiGp(gp),
                                ValueType.CIRCLE, readonly=True)

        self.qEquiGt = QRfParam(self.tr('Equi Gt'),
                                self.dev.equiGt(gt, self.test.gammaS),
                                ValueType.CIRCLE, readonly=True)

        self.qEquiNF = QRfParam(self.tr('Equi NF'),
                                self.dev.equiNF(self.test.NF),
                                ValueType.CIRCLE, readonly=True)

        self.paramsLayout = QGridLayout()
        self.paramsLayout.setSpacing(0)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qGso, 0, 0)
        self.paramsLayout.addWidget(self.qGlo, 1, 0)
        self.paramsLayout.addWidget(line, 0, 1, 2, 1)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qGin, 0, 1)
        self.paramsLayout.addWidget(self.qGout, 1, 1)
        self.paramsLayout.addWidget(line, 0, 2, 5, 1)

        line = QFrame()
        line.setFrameShape(QFrame.HLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(line, 2, 0, 1, 2)
        self.paramsLayout.addWidget(self.qStCircIn, 3, 0, 1, 2)
        self.paramsLayout.addWidget(self.qStCircOut, 4, 0, 1, 2)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qD, 0, 3)
        self.paramsLayout.addWidget(self.qK, 1, 3)
        self.paramsLayout.addWidget(self.qMaxG, 2, 3)
        self.paramsLayout.addWidget(line, 0, 4, 4, 1)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Plain)
        self.paramsLayout.addWidget(self.qGa, 0, 5)
        self.paramsLayout.addWidget(self.qGp, 1, 5)
        self.paramsLayout.addWidget(self.qGt, 2, 5)
        self.paramsLayout.addWidget(self.qNf, 3, 5)
        self.paramsLayout.addWidget(line, 0, 6, 4, 1)

        self.paramsLayout.addWidget(self.qEquiGa, 0, 7)
        self.paramsLayout.addWidget(self.qEquiGp, 1, 7)
        self.paramsLayout.addWidget(self.qEquiGt, 2, 7)
        self.paramsLayout.addWidget(self.qEquiNF, 3, 7)

        self.rootLayout.addLayout(self.paramsLayout)


class QRfItem(QListWidgetItem):

    def __init__(self, item):
        super().__init__(item.name)

        self.item = item


class QRfList(QListWidget):
    itemSelected = Signal(QRfItem)

    def __init__(self):
        super().__init__()

        self.setFixedWidth(160)
        self.itemClicked.connect(self._getItem)

    def addItem(self, item):
        # Check for existing name
        alreadyFound = 0
        items = [self.item(row) for row in range(self.count())]
        while True:
            try:
                copy = next(i for i in items if i.item.name == item.name)
            except StopIteration:
                break
            else:
                if alreadyFound == 0:
                    item.name = item.name + f'_copy({alreadyFound + 1})'
                else:
                    item.name = item.name.replace(
                        f'_copy({alreadyFound})',
                        f'_copy({alreadyFound + 1})'
                    )
                alreadyFound += 1

        it = QRfItem(item)
        super().addItem(it)
        self.setCurrentItem(it)
        self.itemSelected.emit(it)

    def updateItem(self, item):
        items = [self.item(row) for row in range(self.count())]
        it = next(i for i in items if i.item is item)
        it.setText(item.name)

    def removeItem(self, item):
        items = [self.item(row) for row in range(self.count())]
        idx = next(idx for idx, it in enumerate(items) if it.item is item)

        removed = self.takeItem(idx)
        removed.deleteLater()

    def clearSelection(self):
        self.setCurrentItem(None)

    @Slot(str)
    def _getItem(self, item):
        items = [self.item(row) for row in range(self.count())]
        it = next(i for i in items if i.item.name == item.text())
        self.itemSelected.emit(it)


class QRfParam(QWidget):

    def __init__(self, name, val='', valType=ValueType.STRING, scale=0,
                 unit='', readonly=False):
        super().__init__()

        self.type = valType
        if val == float('inf'):
            self.scale = lambda v: v
            self.unscale = self.scale
        elif type(scale) is str and scale == 'dB':
            self.scale = lambda v: 10 * log10(v) if v > 0 else float('-inf')
            self.unscale = lambda v: 10**(v / 10)
        elif type(scale) is int:
            self.scale = lambda v: v * 10**scale
            self.unscale = lambda v: v / 10**scale
        else:
            self.scale = lambda v: v
            self.unscale = self.scale
        self.readonly = readonly

        self.initUi(name, val, unit)

    def initUi(self, name, val, unit):
        if self.readonly:
            self.label = QLabel(self.tr(f'{name}:\t'))
            self.lineEdit = QLabel()
            self.lineEdit2 = QLabel()
        else:
            self.label = QLabel(self.tr(name))
            self.lineEdit = QLineEdit()
            self.lineEdit2 = QLineEdit()

        self.labelUnit = QLabel(self.tr(unit))

        self.label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.labelUnit.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.lineEdit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.lineEdit2.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

        if self.type == ValueType.COMPLEX_RECT:
            self.lineEdit.setFixedWidth(120)

        self.updateVal(val)

        self.layout = QHBoxLayout(self)
        self.layout.setContentsMargins(20, 5, 20, 5)

        self.layout.addWidget(self.label)
        self.layout.addWidget(self.lineEdit)

        if self.type == ValueType.COMPLEX_POLAR \
                or self.type == ValueType.CIRCLE:
            self.layout.addWidget(self.lineEdit2)

        self.layout.addWidget(self.labelUnit)

    def updateVal(self, val):
        if self.type == ValueType.STRING:
            self.lineEdit.setText(val)
        elif self.type == ValueType.NUMBER:
            self.lineEdit.setText(f'{self.scale(val):.3f}')
        elif self.type == ValueType.COMPLEX_RECT:
            self.lineEdit.setText(f'{self.scale(val):.3f}')
        elif self.type == ValueType.COMPLEX_POLAR:
            self.lineEdit.setText(f'{self.scale(abs(val)):.3f}')
            self.lineEdit2.setText(f'{phase(val) * 180 / pi:.1f}\u00b0')
        elif self.type == ValueType.CIRCLE:
            self.lineEdit.setText(
                f'C: {self.scale(abs(val.center)):.3f} '
                f' {phase(val.center) * 180 / pi:.1f}\u00b0'
            )
            self.lineEdit2.setText(f'R: {self.scale(val.radius):.3f}')

    def getValue(self):
        text = self.lineEdit.text()
        text2 = self.lineEdit2.text()

        if self.type == ValueType.STRING:
            return text
        elif self.type == ValueType.NUMBER:
            return self.unscale(float(text))
        elif self.type == ValueType.COMPLEX_RECT:
            return self.unscale(complex(text))
        elif self.type == ValueType.COMPLEX_POLAR:
            mod = self.unscale(float(text.strip()))
            ph = float([e.strip() for e in split('[°]+', text2)][0])
            return rect(mod, ph * pi / 180)
