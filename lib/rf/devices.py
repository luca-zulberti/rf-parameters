from cmath import rect, phase
from math import sqrt


class Circle:

    def __init__(self, center, radius):
        self.center = center
        self.radius = radius


class Dev:

    def __init__(self, name=''):
        self.name = name


class YDev(Dev):

    def __init__(self, yi=0.0, yr=0.0, yf=0.0, yo=0.0, name=''):
        super().__init__(name)

        self._yi = yi
        self._yr = yr
        self._yf = yf
        self._yo = yo

        self._updateParameters()

    @property
    def yi(self):
        return self._yi

    @yi.setter
    def yi(self, value):
        self._yi = value
        self._updateParameters()

    @property
    def yr(self):
        return self._yr

    @yr.setter
    def yr(self, value):
        self._yr = value
        self._updateParameters()

    @property
    def yf(self):
        return self._yf

    @yf.setter
    def yf(self, value):
        self._yf = value
        self._updateParameters()

    @property
    def yo(self):
        return self._yo

    @yo.setter
    def yo(self, value):
        self._yo = value
        self._updateParameters()

    def _updateParameters(self):
        yrf = self.yr * self.yf

        try:
            gsopt = sqrt((2 * self.yi.real * self.yo.real
                          - yrf.real)**2 - abs(yrf)**2)
        except ValueError:
            gsopt = float('inf')

        try:
            gsopt = gsopt / (2 * self.yo.real)
            bsopt = - self.yi.imag + yrf.imag / (2 * self.yo.real)
            self.ysopt = gsopt + bsopt * 1j
        except ZeroDivisionError:
            self.ysopt = float('inf')

        try:
            glopt = gsopt * self.yo.real / self.yi.real
            blopt = - self.yo.imag + yrf.imag / (2 * self.yi.real)
            self.ylopt = glopt + blopt * 1j
        except ZeroDivisionError:
            self.ylopt = float('inf')

        try:
            self.c = abs(yrf) / (2 * self.yi.real * self.yo.real - yrf.real)
        except ZeroDivisionError:
            self.c = float('inf')

    def gp(self, yl):
        try:
            ret = abs(self.yf / (self.yo + yl))**2 \
                * yl.real / self.yin(yl).real
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def ga(self, ys):
        d1 = (self.yo * ys + self.yo * self.yi - self.yr * self.yf)
        d2 = (self.yi + ys).conjugate()

        try:
            ret = abs(self.yf)**2 * ys.real / (d1 * d2).real
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def gt(self, ys, yl):
        d = abs((self.yo + yl) * (self.yi + ys) - self.yr * self.yf)**2

        try:
            ret = 4 * ys.real * yl.real * abs(self.yf)**2 / d
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def yin(self, yl):
        yrf = self.yr * self.yf

        try:
            ret = self.yi - yrf / (self.yo + yl)
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def yout(self, ys):
        yrf = self.yr * self.yf

        try:
            ret = self.yo - yrf / (self.yi + ys)
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def k(self, ys, yl):
        yrf = self.yr * self.yf
        n = 2 * (self.yi.real + ys.real) * (self.yo.real + yl.real)

        try:
            ret = n / (yrf.real + abs(yrf))
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def betaA(self, ys, yl):
        yrf = self.yr * self.yf
        d = (self.yi + ys) * (self.yo + yl)

        try:
            ret = yrf / d
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def aV(self, yl):
        try:
            ret = self.yf / (self.yo + yl)
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def aI(self, yl):
        try:
            ret = (self.yf + self.yo * self.aV(yl)) / self.yin(yl)
        except ZeroDivisionError:
            ret = float('inf')

        return ret


class SDev(Dev):

    def __init__(self, s11=0.0, s21=0.0, s12=0.0, s22=0.0, NFmin=1.0, Rn=0.0,
                 gammaSoptN=0.0, name=''):
        super().__init__(name)

        self._s11 = s11
        self._s21 = s21
        self._s12 = s12
        self._s22 = s22
        self._NFmin = NFmin
        self._Rn = Rn
        self._gammaSoptN = gammaSoptN

        self._updateParameters()

    @property
    def s11(self):
        return self._s11

    @s11.setter
    def s11(self, value):
        self._s11 = value
        self._updateParameters()

    @property
    def s12(self):
        return self._s12

    @s12.setter
    def s12(self, value):
        self._s12 = value
        self._updateParameters()

    @property
    def s21(self):
        return self._s21

    @s21.setter
    def s21(self, value):
        self._s21 = value
        self._updateParameters()

    @property
    def s22(self):
        return self._s22

    @s22.setter
    def s22(self, value):
        self._s22 = value
        self._updateParameters()

    @property
    def NFmin(self):
        return self._NFmin

    @NFmin.setter
    def NFmin(self, value):
        self._NFmin = value
        self._updateParameters()

    @property
    def Rn(self):
        return self._Rn

    @Rn.setter
    def Rn(self, value):
        self._Rn = value
        self._updateParameters()

    @property
    def gammaSoptN(self):
        return self._gammaSoptN

    @gammaSoptN.setter
    def gammaSoptN(self, value):
        self._gammaSoptN = value
        self._updateParameters()

    def _updateParameters(self):
        self.D = self.s11 * self.s22 - self.s21 * self.s12
        try:
            self.K = (1 - abs(self.s11)**2 - abs(self.s22)**2
                      + abs(self.D)**2) / (2 * abs(self.s12 * self.s21))
        except ZeroDivisionError:
            self.K = float('inf')

        C1 = self.s11 - self.s22.conjugate() * self.D
        C2 = self.s22 - self.s11.conjugate() * self.D
        self.C1 = C1
        self.C2 = C2

        try:
            center = C1.conjugate() / (abs(self.s11)**2 - abs(self.D)**2)
            radius = abs(self.s12 * self.s21) \
                / abs(abs(self.D)**2 - abs(self.s11)**2)
        except ZeroDivisionError:
            center = float('inf')
            radius = float('inf')

        self.inputSC = Circle(center, radius)

        try:
            center = C2.conjugate() / (abs(self.s22)**2 - abs(self.D)**2)
            radius = abs(self.s12 * self.s21) \
                / abs(abs(self.D)**2 - abs(self.s22)**2)
        except ZeroDivisionError:
            center = float('inf')
            radius = float('inf')

        self.outputSC = Circle(center, radius)

        if (self.K > 1):
            B1 = 1 + abs(self.s11)**2 - abs(self.s22)**2 - abs(self.D)**2
            B2 = 1 - abs(self.s11)**2 + abs(self.s22)**2 - abs(self.D)**2

            try:
                mod = B1 / (2 * abs(C1)) - sqrt(abs(B1)
                                                ** 2 / (4 * abs(C1)**2) - 1)
            except ZeroDivisionError:
                self.gammaSopt = float('inf')
            else:
                self.gammaSopt = rect(mod.real, phase(C1.conjugate()))

            try:
                mod = B2 / (2 * abs(C2)) - sqrt(abs(B2)
                                                ** 2 / (4 * abs(C2)**2) - 1)
            except ZeroDivisionError:
                self.gammaLopt = float('inf')
            else:
                self.gammaLopt = rect(mod.real, phase(C2.conjugate()))

            val = self.K - sqrt(self.K**2 - 1) if abs(self.D) < 1 else \
                self.K + sqrt(self.K**2 - 1)

            try:
                self.maxG = abs(self.s21)**2 * val / abs(self.s12 * self.s21)
            except ZeroDivisionError:
                self.maxG = float('inf')
        else:
            self.gammaSopt = float('nan')
            self.gammaLopt = float('nan')
            self.maxG = float('nan')

    def gammaIn(self, gammaL):
        try:
            ret = self.s11 + self.s12 * self.s21 * gammaL \
                / (1 - self.s22 * gammaL)
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def gammaOut(self, gammaS):
        try:
            ret = self.s22 + self.s12 * self.s21 * gammaS \
                / (1 - self.s11 * gammaS)
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def ga(self, gammaS):
        gammaOut = self.gammaOut(gammaS)
        return self.gt(gammaS, gammaOut.conjugate())

    def gp(self, gammaL):
        gammaIn = self.gammaIn(gammaL)
        try:
            ret = abs(self.s21)**2 * (1 - abs(gammaL)**2) \
                / (abs((1 - self.s22 * gammaL))**2 * (1 - abs(gammaIn)**2))
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def gt(self, gammaS, gammaL):
        try:
            ret = (1 - abs(gammaS)**2) * abs(self.s21)**2 \
                * (1 - abs(gammaL)**2) \
                / abs((1 - self.s11 * gammaS) * (1 - self.s22 * gammaL)
                      - self.s12 * self.s21 * gammaS * gammaL)**2
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def nf(self, gammaS):
        n = 4 * self.Rn * abs(gammaS - self.gammaSoptN)**2
        d = (1 - abs(gammaS)**2) * abs(1 + self.gammaSoptN)**2
        try:
            ret = self.NFmin + n / d
        except ZeroDivisionError:
            ret = float('inf')

        return ret

    def equiGa(self, Ga):
        try:
            ga = Ga / abs(self.s21)**2
        except ZeroDivisionError:
            return Circle(float('inf'), float('inf'))
        den = 1 + ga * (abs(self.s11)**2 - abs(self.D)**2)
        Ms12 = abs(self.s12 * self.s21)

        try:
            center = ga * self.C1.conjugate() / den
            radius = sqrt(1 - 2 * self.K * Ms12 * ga
                          + (ga * Ms12)**2) / abs(den)
        except (ZeroDivisionError, ValueError):
            center = float('inf')
            radius = float('inf')

        return Circle(center, radius)

    def equiGp(self, Gp):
        try:
            gp = Gp / abs(self.s21)**2
        except ZeroDivisionError:
            return Circle(float('inf'), float('inf'))

        den = 1 + gp * (abs(self.s22)**2 - abs(self.D)**2)
        Ms12 = abs(self.s12 * self.s21)

        try:
            center = gp * self.C2.conjugate() / den
            radius = sqrt(1 - 2 * self.K * Ms12 * gp
                          + (gp * Ms12)**2) / abs(den)
        except (ZeroDivisionError, ValueError):
            center = float('inf')
            radius = float('inf')

        return Circle(center, radius)

    def equiGt(self, Gt, gammaS):
        try:
            gt = Gt * abs(1 - self.s11 * gammaS)**2 / abs(self.s21)**2 \
                / (1 - abs(gammaS)**2)
        except ZeroDivisionError:
            return Circle(float('inf'), float('inf'))
        gammaOut = self.gammaOut(gammaS)
        den = 1 + gt * abs(gammaOut)**2

        try:
            center = gt * gammaOut.conjugate() / den
            radius = sqrt((1 - gt) / den + (gt * abs(gammaOut))
                          ** 2 / abs(den)**2)
        except (ZeroDivisionError, ValueError):
            center = float('inf')
            radius = float('inf')

        return Circle(center, radius)

    def equiNF(self, NF):
        try:
            nf = (NF - self.NFmin) * \
                abs(1 + self.gammaSoptN)**2 / (4 * self.Rn)
        except ZeroDivisionError:
            return Circle(float('inf'), float('inf'))

        den = 1 + nf

        try:
            center = self.gammaSoptN / den
            radius = sqrt((nf - abs(self.gammaSoptN)**2) / den
                          + abs(self.gammaSoptN)**2 / abs(den)**2)
        except (ZeroDivisionError, ValueError):
            center = float('inf')
            radius = float('inf')

        return Circle(center, radius)


class Test():

    def __init__(self, name=''):
        self.name = name


class YTest(Test):

    def __init__(self, ys=0.02, yl=0.02, name=''):
        super().__init__(name)
        self.ys = ys
        self.yl = yl


class STest(Test):

    def __init__(self, gammaS=0.0, gammaL=0.0, Ga=1.0, Gp=1.0, Gt=1.0,
                 NF=1.0, name=''):
        super().__init__(name)
        self.gammaS = gammaS
        self.gammaL = gammaL
        self.Ga = Ga
        self.Gp = Gp
        self.Gt = Gt
        self.NF = NF
