#!/usr/bin/env python3
from cmath import rect, pi, phase
from math import log10

from lib.rf.sparam import SDev


def cpolar(c):
    return f'{abs(c):.3f} / {phase(c) * 180 / pi:.2f}\u00b0'


# s11 = rect(0.68, -130 * pi / 180)
# s21 = rect(6.1, 102 * pi / 180)
# s12 = rect(0.09, 29 * pi / 180)
# s22 = rect(0.43, -64 * pi / 180)

s11 = rect(0.68, -167 * pi / 180)
s21 = rect(3.3, 79 * pi / 180)
s12 = rect(0.1, 22 * pi / 180)
s22 = rect(0.29, 77 * pi / 180)

NFmin = 10**(1.5 / 10)
Rn = 0.15
gammaSoptN = rect(0.48, 134 * pi / 180)

gammaS = rect(0.3, 95 * pi / 180)
gammaL = rect(0.4, 45 * pi / 180)

Ga = 2.5
Gp = 4.2
Gt = 8.3
NF = 1.9

dev = SDev(s11, s21, s12, s22, NFmin, Rn, gammaSoptN)

print(f'S11:\t{cpolar(s11)}')
print(f'S21:\t{cpolar(s21)}')
print(f'S12:\t{cpolar(s12)}')
print(f'S22:\t{cpolar(s22)}')

print('')

print(f'\u0393s:\t{cpolar(gammaS)}')
print(f'\u0393L:\t{cpolar(gammaL)}')

print('')

print(f'\u0393s opt:\t{cpolar(dev.gammaSopt)}')
print(f'\u0393l opt:\t{cpolar(dev.gammaLopt)}')

print('')

print(f'\u0393in:\t{cpolar(dev.gammaIn(gammaL))}')
print(f'\u0393out:\t{cpolar(dev.gammaOut(gammaS))}')

print('')

print(f'D:\t{cpolar(dev.D)}')
print(f'K:\t{dev.K:.3f}')
print(f'\u0393in:\t{cpolar(dev.gammaIn(gammaL))}')
print(f'\u0393out:\t{cpolar(dev.gammaOut(gammaS))}')
print(f'Ga:\t{10 * log10(dev.ga(gammaS)):.3f} dB')
print(f'Gp:\t{10 * log10(dev.gp(gammaL)):.3f} dB')
print(f'Gt:\t{10 * log10(dev.gt(gammaS, gammaL)):.3f} dB')
print(f'maxG:\t{10 * log10(dev.maxG):.3f} dB')

print('')

print(f'NFmin:\t{10 * log10(NFmin):.3f} dB')
print(f'Rn:\t{Rn:.3f}')
print(f'\u0393s on:\t{cpolar(gammaSoptN)}')
print(f'NF:\t{10 * log10(dev.nf(gammaS)):.3f} dB')

print('')

print(f'in StabCirc:\tC: {cpolar(dev.inputSC.center)}'
      f'\tR: {dev.inputSC.radius:.3f}')
print(f'out StabCirc:\tC: {cpolar(dev.outputSC.center)}'
      f'\tR: {dev.outputSC.radius:.3f}')

print('')

circle = dev.equiGa(10**(Ga / 10))
print(f'equi Ga at {Ga:.3f} dB'
      f'\tC: {cpolar(circle.center)}\tR: {circle.radius:.3f}')
circle = dev.equiGp(10**(Gp / 10))
print(f'equi Gp at {Gp:.3f} dB'
      f'\tC: {cpolar(circle.center)}\tR: {circle.radius:.3f}')
circle = dev.equiGt(10**(Gt / 10), gammaS)
print(f'equi Gt at {Gt:.3f} dB'
      f'\tC: {cpolar(circle.center)}\tR: {circle.radius:.3f}')
circle = dev.equiNF(10**(NF / 10))
print(f'equi NF at {NF:.3f} dB'
      f'\tC: {cpolar(circle.center)}\tR: {circle.radius:.3f}')
